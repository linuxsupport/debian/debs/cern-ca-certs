# SPDX-FileCopyrightText: 2022 CERN (home.cern)

USE_HASH_OLD ?= n

DESTDIR ?=
prefix ?= /usr/local
exec_prefix ?= $(prefix)
sbindir ?= $(exec_prefix)/sbin
sysconfdir ?= /etc
datadir ?= $(prefix)/share

CERTS_DIR ?= $(sysconfdir)/pki/tls/certs
ANCHORS_CERTS_DIR ?= $(datadir)/pki/ca-trust-source/anchors/
OPENLDAP_CERTS_DIR ?= $(sysconfdir)/openldap/cacerts

CRT := $(shell ls -1 *.crt | grep -v bundle)
PEM := $(CRT:.crt=.pem)
SIGNING_POLICY := $(CRT:.crt=.signing_policy)

all: build

build: pem bundles signing_policy
pem: $(PEM)
$(PEM): %.pem: %.crt
	openssl x509 -inform DER -in '$<' -outform PEM -out '$@'
	ln -sf '$@' $$(openssl x509 -in '$@' -noout -subject_hash).0
ifeq ($(USE_HASH_OLD),y)
	ln -sf '$@' $$(openssl x509 -in '$@' -noout -subject_hash_old).0
endif

check: test

.ONESHELL:
test: $(PEM)
	for pem in $(patsubst %,'%',$^)
	do
		echo $${pem}
		test $$(cat $${pem} | grep -E 'BEGIN.* CERTIFICATE' | wc -l) -gt 1 && exit 7
	done
	exit 0

# TODO
# In the equivalent RPM spec script, this task is done only if "CERN"
# is in the certificte name. However, today, all certificates have it.
signing_policy: $(SIGNING_POLICY)
$(SIGNING_POLICY): %.signing_policy: %.pem
	$(eval SUBJECT:=$(shell openssl x509 -subject -noout -in '$<' | sed -e 's/subject= *//'))
	echo "access_id_CA      X509         '$(SUBJECT)'" > '$@'
	echo "pos_rights        globus        CA:sign" >> '$@'
	echo "cond_subjects     globus     '\"/DC=ch/DC=cern/*\"'" >> '$@'
	ln -sf '$@' $$(openssl x509 -in '$<' -noout -subject_hash).signing_policy
ifeq ($(USE_HASH_OLD),y)
	ln -sf '$@' $$(openssl x509 -in '$<' -noout -subject_hash_old).signing_policy
endif

bundles: CERN-bundle.crt CERN-bundle.pem
CERN-bundle.crt: $(CRT)
	cat $(patsubst %,'%',$^) > $@

CERN-bundle.pem: $(PEM)
	cat $(patsubst %,'%',$^) > $@

clean:
	rm -f *.pem *.pem.subject *.0 *.signing_policy CERN-bundle.*

install: install_pki_anchors install_certs install_openldap

install_openldap:
# only *.pem certs in openldap cacerts: no signing policy .. etc
	$(MAKE) install_pem PEM_DEST=$(OPENLDAP_CERTS_DIR)
# and hashes: openldap lib uses hashes
	$(MAKE) install_pem_hash PEM_HASH_DEST=$(OPENLDAP_CERTS_DIR)

install_pki_anchors:
	$(MAKE) install_crt CRT_DEST=$(ANCHORS_CERTS_DIR)

install_certs:
	$(MAKE) install_crt CRT_DEST=$(CERTS_DIR)
	$(MAKE) install_pem PEM_DEST=$(CERTS_DIR)
	$(MAKE) install_pem_hash PEM_HASH_DEST=$(CERTS_DIR)
	install -m 0444 *.signing_policy $(DESTDIR)$(CERTS_DIR)/

install_crt: $(CRT) CERN-bundle.crt
	mkdir -p $(DESTDIR)/$(CRT_DEST)
	install -m 0444 $(patsubst %,'%',$^) $(DESTDIR)/$(CRT_DEST)

install_pem: $(PEM) CERN-bundle.pem
	mkdir -p $(DESTDIR)/$(PEM_DEST)
	install -m 0444 $(patsubst %,'%',$^) $(DESTDIR)/$(PEM_DEST)

install_pem_hash: $(PEM) CERN-bundle.pem
	mkdir -p $(DESTDIR)/$(PEM_HASH_DEST)
	install -m 0444 *.0 $(DESTDIR)/$(PEM_HASH_DEST)

.PHONY: all build bundles clean install pem signing_policy test
.PHONY: install_crt install_pem install_pem_hash
.PHONY: install_pki_anchors install_certs install_openldap
